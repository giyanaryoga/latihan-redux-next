export default {
  all: () =>
    Promise.resolve([
      {
        id: 1,
        name: "Ryan Gosling",
        address: "Los Feliz, California, U.S",
        phoneNumber: "+1-123-123",
        photo:
          "https://cdn.famousoutfits.com/wp-content/uploads/2014/08/profile-ryan-gosling.png",
      },
      {
        id: 2,
        name: "Robert Pattinson",
        address: "Orlando, Florida, U.S",
        phoneNumber: "+1-123-132",
        photo:
          "https://m.media-amazon.com/images/M/MV5BNzk0MDQ5OTUxMV5BMl5BanBnXkFtZTcwMDM5ODk5Mg@@._V1_UY317_CR12,0,214,317_AL_.jpg",
      },
      {
        id: 3,
        name: "Christian Bale",
        address: "Toronto, Canada",
        phoneNumber: "+1-123-456",
        photo:
          "https://upload.wikimedia.org/wikipedia/commons/thumb/0/0a/Christian_Bale-7837.jpg/220px-Christian_Bale-7837.jpg",
      },
      {
        id: 4,
        name: "Yoga",
        address: "Cirebon, Jabar",
        phoneNumber: "+62-827354829",
        photo: "",
      },
    ]),
};
